## CNCUtil - CNC GCODE Generator

CNCUtil converts high level scripts into low level GCode.  This allows a few lines of specification to produce thousands of high quality GCode commands. I wrote CNCUtil because it allows me to produce GCode similar to GCODE produced by expert machinists.   Good GCODE can save a lot of milling time when compared to the GCODE produced by 3D CAD to GCODE translators.

The largest number of users seem to be [blind machinists](http://www.bing.com/search?q=blind+machinists) who seem to think in scripting and can not easily interact with 3D CAD displays.

CNCUtil delivers built in features for drilling, milling circular, rectangular and multi-sided pockets, Arc pockets, etc. CNCUtil does most of the math which saves a lot of time.

CNCUtil allows Machinists to retain the control of the milling machine where it is needed while allowing the computer to do the hard work of producing the GCODE needed for things like circular

### Example ###

    require 'CNCMill'  
    require 'CNCShapeCircle'
    aMill = CNCMill.new()
    aMill.job_start()  
    aCircle = CNCShapeCircle.new(aMill) 
    aMill.home()
    #   Param = center-x,center-y,diam,depth,island_diam)
    aCircle.mill_pocket(0.0, 1.5, 2.5,  -0.75, 0.0)    #large circle no island
    aCircle.mill_pocket(0.0, 1.5, 2.5,  -0.85, 0.75)   #deeper ring with Island
    aCircle.mill_pocket(0.0, 1.5, 0.25, -0.95, 0.0)    #drill through the center
    aMill.home() # implies a retract()
    aMill.job_finish()



### Metadata

* License is [MIT](license.txt)
* Version: 1.0

### How do I get set up? ###

> You may need to download the repository to a local disk and then open the website and users guide with your browser.  It was tested as served by apache. 

* [Web Site](website/index.html)


* [cncutil-users-guide](website/cncutil-users-guid.html)

### Contribution guidelines ###

* see [todo.md](todo.md)
* see the [old web site](website) which needs to be converted to a bunch of MD files or to the Bitbucket wiki.


### Who do I talk to? ###

* Repo owner:  Joe Ellsworth

  > I wrote CNCUtil for some of my own work.  I used it extensive but mostly for relatively simple parts.   I can not offer free support at the current time.  I could add features, and provide  support but would have to charge for consulting services.  [Contact](http://BayesAnalytic.com/contact)

* I have completed the hard engineering portion of CNCUtil. At this point, I think the community will better served if steered by a group of [blind machinists](http://www.google.com/?q=blind+machinists/#output=search&sclient=psy-ab&q=blind+machinists&oq=blind+machinists&biw=912&bih=804) who are experts in their field. 

* Please contact me If you are willing to adopt CNCUtil for long term open source maintenance and have some experience building a community around open source products.  


>#### Some of my other sites####

>* **[Solve Water Scarcity Using DEM](http://AirSolarWater.com/dem) **  A Novel way of using micro reservoirs to reduce the impact of water scarcity. Ideal for adoption on poor countries especially in the very poor rural agricultural regions.   It is based on the premis of building very small micro capture dams using stones and dam.   The DEM (Digital Elevation Model) work models water flow so we can show people where to build these small reservoirs so each reservoir will refill with 1,000's of gallons of water everytime there is more than 0.3 inches of runoff.  Water soaks in to nurture food producing trees while also refilling local aquifer.
>* **[Quantized Classifier](https://bitbucket.org/joexdobs/ml-classifier-gesture-recognition)** A Machine Learning classifier using novel techniques that delivers precision at 100% recall comparable to with Deep Learning CNN for many classification tasks while running many times faster.  If is written in GO  and available on a MIT license. 
>
>
>* **[bayesanlytic.com ML Stock Trading engine](http://bayesanalytic.com/)** Is Machine Learning predictive Analytic engine using AI techniques with high volume, high speed and big data capability. Designed to support stock and option trading by identifying which trades with the highest probability of meeting profitability goals in a specific time frame. May also be used to increase marketing ROI by targeting users most likely to find a specific offer attractive. The engine can reduce wasted money contacting people who have little or no change of buying. One articles shows the [Another article shows that ](http://bayesanalytic.com/how-days-purchased-can-affect-option-sucess/)[Distribtion rank can be used as a purchase filter for options](http://cncutil.org/Distribution%20Rank%20as%20an%20Option%20Purchase%20filter) Finally there is an article that shows [what do do when traders experience losses](http://bayesanalytic.com/when-good-traders-loose/)
>
>
>* [The **Air Solar Water product line A2WH**](http://airsolarwater.com/)  is a fully renewable extraction of water from air.  Provides systems which extract liquid potable water from air using solar energy.   This technology  can deliver water cost effectively in the most hostile locations and can scale from 1 gallon per day up through millions of gallons per day. A2WH patented technology provides world leading ability to extract water from air using only renewable energy. The new 2013 model of [Grow Dryland](http://airsolarwater.com/grow-dryland-overview/) can start trees and keep the seedlings alive in the the driest desert even when the seedlings would normally die of thirst. A2WH which will someday be applied to [sequester more carbon](http://airsolarwater.com/reclaim-unproductive-desert-land/) than the USA emitts every year and provides the the ideal way to [reduce the financial impact of drought on ranchers](http://airsolarwater.com/reduce-impact-of-drought-on-cattle-and-sheep-ranching/) 
>
>
>* [**Correct Energy Solutions**](http://correctenergysolutions.com/) -  provides  unique energy solutions designed solve real world energy and conservation problems.  This includes [micro-wind turbines](http://correctenergysolutions.com/wind) suitable for near ground installation,  renewable cooling and air to water technologies.  
>
>
>* My personal site [**JoeEllsworth.com**](http://joeellsworth.com/) which contains my [resume](http://joeellsworth.com/resume/2013-v04-joe-bio-dir-cto-architect.pdf)

