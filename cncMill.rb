# cncMill.rb
#
# #  See (C) notice in http://cncutil.org/cnc/CNCUtil/license.txt for license and copyright.


require 'cncBit'
require 'cncMaterial'
require 'cncGeometry'
require 'cncExtent'

# Class to be filled in for the mill which
# gives us a conversion of  F# type speeeds
# of Feet Per Minute.  This is needed To
# map our calculations for optimal speeds
# to an approapriate feed rate.
#

###################################
class CNCFeedSpeeds
###################################
 


end #class


# *****************************************
class CNCMill 
# *****************************************
   # - - - - - - - - - - - - - - - -   
   def initialize(config_file_name=nil, material_file_name=nil)
   # - - - - - - - - - - - - - - - -
     @cz          = 0.0
     @cx          = 0.0
     @cy          = 0.0
     @speed_curr  = 15
     @speed_fast  = 15
     @speed_plung = 5
     @speed_normal= 12
     @speed_max   = 20
     @speed_finish   = 10
     @curr_bit    = CNCBit.new
     @max_x_move  = 14
     @min_x       = -7
     @max_x       = @max_x_move - @min_x
     @max_y       = 3.90
     @min_y       = -0.35
     @max_z       = 4.0
     @min_z       = -4.0
     @mill_depth  = -0.2
     @retract_depth = 0.1
     @no_move_count = 0

     @spindle_rpm = [10839, 6513, 4365, 2340, 1975, 1200]
       # RPM of spindel in Taig 2019 with belt positions starting
       # at the top and working towards the bottom.  This mill
       # seems to have a problem reaching full speed in the highest
       # position while all others seem to work OK.  These numbers are used
       # to calculate what belt postion to request based on the work material
       # and bit being used


     @fpm = []
       # This array maps the different F speeds to
       # Feet per min for the given mill, controller, stepper
       # motor combination.  This is used to map our calculations
       # for optimal feed rates into an approapriate feed.

     @ipm = [] 
       # Inches per minute at various F speeds
       # calculated from test_speed.

     @test_speed = [82,44,30,21,19,15,12,11,9,8,7.5,7,6.5,6.4,6.2,5.5,5.4,5.3,4.5,4]
       # empiracle test of speed of mill to move the X,Y axis one inch
       # at the same time.  Starting in F1 working up through F20
       # results are in number of seconds required for the move.  This
       # value is used to create a speed of feet per minute for the
       # various feed speeds
      
     # computer inches per minute movement at
     # various F speeds.
     cc = 0
     for aNoSec in @test_speed
       cc += 1
       inch_per_sec  = 1.0 / Float(aNoSec)
       inch_per_min  = inch_per_sec * 60.0
       feet_per_min = inch_per_min / 12.0
       @fpm.push(feet_per_min)
       @ipm.push(inch_per_min)
       print "(F", cc, "FPM=", feet_per_min, " test speed=", aNoSec,")\n"
     end #for
     
   end  # init 

   # - - - - - - - - - - - - - -
   def job_start
   # - - - - - - - - - - - - - -
     print "%\n"
   end 

   # - - - - - - - - - - - - - -
   def job_finish
   # - - - - - - - - - - - - - -
     print "%\n"
   end 

   ###########
   def pause(message=nil)
   ##########
     if (message == nil)
        message = "PAUSE"
     end #if
     print "\nG01 (", message, ")\n\n"
   end  #meth
   
   ###########
   def mount_bit(bit_num, message=nil)
   ##########
     if (message != nil)
        pause(message)
     end #if
     print "\nT", bit_num, " G06\n"
   end  #meth
   


   # - - - - - - - - - - - - - - - -   
   def move(xo,yo=@cy,zo=@cz,so=@speed_curr)
   # - - - - - - - - - - - - - - - -
     #print "( move xo=", xo, " yo=",yo,  " zo=", zo,  " so=", so, ")\n"
     if (xo ==@cx) && (yo == @cy) && (zo == @cz)
       #print "(move - already positioned)\n"
       @no_move_count += 1
     else
       if (xo > @max_x)
         print "(move x=", sprintf("%8.3f",xo), " GT max of ", @max_x, ")\n"
         xo = @max_x
       elsif (xo < @min_x)
         print "(move x=", sprintf("%8.3f",xo), " LT min of ", @min_x, ")\n"
         xo = @min_x
       end #if
       if (yo > @max_y)
         print "(move y=", sprintf("%8.3f",yo), " GT max of ", @max_y, ")\n"
         yo = @max_y
       elsif (yo < @min_y)
         print "(move y=", sprintf("%8.3f",yo), " LT min of ", @min_y, ")\n"
         yo = @min_y
       end #if
       if (zo > @max_z)
         print "(move z=", sprintf("%8.3f",zo), " GT max of ", @max_z, ")\n"
         zo = @max_z
       elseif (zo < @min_z)
         print "(move x=", sprintf("%8.3f",zo), " LT min of ", @min_z, ")\n"
         zo = @min_z
       end #if



       if ((xo != @cx) && (yo != @cy) && (zo != @cz))
         print "G01 X", sprintf("%8.3f", xo), " Y", sprintf("%8.3f", yo)," Z", sprintf("%8.3f", zo), " F", so, "\n"
       elsif ((xo != @cx) && (yo != @cy))
         print "G01 X", sprintf("%8.3f", xo), " Y",sprintf("%8.3f", yo)," F", so, "\n"
       elsif ((xo != @cx) && (zo != @cz))
         print "G01 X", sprintf("%8.3f", xo), " Z",sprintf("%8.3f", zo)," F", so, "\n"
       elsif ((yo != @cy) && (zo != @cz))
         print "G01 Y", sprintf("%8.3f", yo), " Z",sprintf("%8.3f", zo)," F", so, "\n"
       elsif (xo != @cx) 
         print "G01 X",sprintf("%8.3f", xo)," F", so, "\n"
       elsif (yo != @cy) 
         print "G01 Y",sprintf("%8.3f", yo)," F", so, "\n"
       elsif (zo != @cz) 
         print "G01 Z",zo," F", so, "\n"
       else
         print "G01 X", sprintf("%8.3f", xo), " Y",sprintf("%8.3f", yo)," Z",sprintf("%8.3f", zo)," F", so, "\n"
       end
         
       @cx = xo
       @cy = yo
       @cz = zo
     end #if
   end #meth


   # - - - - - - - - - - - - - - - - - -
   def cz
   # - - - - - - - - - - - - - - - - - -  
     @cz
   end #meth

   # - - - - - - - - - - - - - - - - - -
   def cx
   # - - - - - - - - - - - - - - - - - -  
     @cx
   end #meth

   # - - - - - - - - - - - - - - - - - -
   def cy
   # - - - - - - - - - - - - - - - - - -  
     @cy
   end #meth


   # - - - - - - - - - - - - - - - -     
   def current_bit(aBit=nil)
   # - - - - - - - - - - - - - - - -      
     if aBit != nil
       @curr_bit = aBit
     end #if
     @curr_bit
   end

   # - - - - - - - - - - - - - - - -
   def bit_radius
   # - - - - - - - - - - - - - - - -
     @curr_bit.radius
   end #meth

   # - - - - - - - - - - - - - - - -
   def bit_diam
   # - - - - - - - - - - - - - - - -
     @curr_bit.diam
    end #meth

  # - - - - - - - - - - - - - - - -
  def flute_len
  # - - - - - - - - - - - - - - - -  
     @curr_bit.flute_len
  end #meth

  # - - - - - - - - - - - - - - - -
  def cut_increment
  # - - - - - - - - - - - - - - - -
    # TODO: Modify this so that it gets
    #   the cut increment based on current
    #   speed setting finish or rough
    #   and material selected.  The cutting
    #   bit should calculate this by using
    #   the material that it can obtain
    #   from the mill.
    return @curr_bit.cut_increment_rough
   end #meth


   # - - - - - - - - - - - - - - - -
  def cut_increment_rough
  # - - - - - - - - - - - - - - - -
    @curr_bit.cut_increment_rough
   end #meth

   # - - - - - - - - - - - - - - - -
   def cut_increment_finish
   # - - - - - - - - - - - - - - - -  
     @curr_bit.cut_increment_finish
   end #meth 


   # Returns the current cutting 
   # depth increment.  This is based
   # on the current material, type 
   # of bit,  size of mill and 
   # whether finish cutting or rough
   # cutting.   The mill will make this
   # number have to make a number of
   # passes to make deeper cuts and the
   # total number is generally calculated
   # by dividing total depth of cut by
   # this number.   The method 
   # set_speed_finish will generally cause
   # this method to returrn a smaller number
   # than when set_speed_rough is used.
   # - - - - - - - - - - - - - - - -
   def cut_depth_inc_curr
   # - - - - - - - - - - - - - - - -  
     return @curr_bit.cut_depth_inc_curr
     # TODO: enhance this so that it
     #   is using bit and material
     #   knowledge to determine proper
     #   cut depth.
   end #meth 

   # - - - - - - - - - - - - - - - -
   def cut_depth_rough
   # - - - - - - - - - - - - - - - -  
     @curr_bit.cut_depth_rough
   end #meth 

   # - - - - - - - - - - - - - - - -
   def cut_depth_finish
   # - - - - - - - - - - - - - - - -  
     @curr_bit.cut_depth_finish
   end #meth 


   # - - - - - - - - - - - - - - - -     
   def move_y(yo, zo=@cz, so=@speed_curr)
   # - - - - - - - - - - - - - - - -     
     move(@cx,yo,zo,so)
   end #meth
     
     
   # - - - - - - - - - - - - - - - -     
   def move_z(zo, so=@speed_curr)
   # - - - - - - - - - - - - - - - -     
     move(@cx,@cy,zo,so)
   end #meth
     
     
   # - - - - - - - - - - - - - - - -     
   def retract(depth = @retract_depth)
   # - - - - - - - - - - - - - - - -     
     if (depth == nil)
       depth = @retract_depth
     end #if
     if (@cz == depth)
       @no_move_count += 1
     else
       if (depth > @max_z)
         depth = @max_z
       elsif (depth < @min_z)
         depth = @min_z
       end #else
      
       print "G00 Z",sprintf("%8.3f", depth) , " (retract) \n"
       @cz = depth
     end #else
   end #meth


   # - - - - - - - - - - - - - - - -
   def plung(depth = @mill_depth)
   # - - - - - - - - - - - - - - - -     
     if (mill_depth == @cz)
        @no_move_count += 1
     else
       if (depth > @max_z)
         depth = @max_z
       elsif (depth < @min_z)
         depth = @min_z
       end #if

       print "G01 Z", sprintf("%8.3f", depth), " F", @speed_plung, " (plung)\n"
       @cz = depth
     end #if
   end #meth



   # - - - - - - - - - - - - - - - -     
   def  mill_depth(depth=nil)
   # - - - - - - - - - - - - - - - -     
     if (depth != nil)
        if (depth < @max_z)
           depth =   @max_z
        elsif (depth < @min_z)
           depth = @min_z
        end #if
        @mill_depth = depth
     end #if
     @mill_depth
   end #meth
    
   # - - - - - - - - - - - - - - - -    
   def speed
   # - - - - - - - - - - - - - - - -     
     return @speed_curr 
   end # meth
    

   # - - - - - - - - - - - - - - - -    
   def set_speed(speed)
   # - - - - - - - - - - - - - - - -     
     if (speed != nil)
       if ((speed > 0) && (speed < @speed_max))
         @speed_curr = speed
       end #if
     end #if
   end # meth


   # - - - - - - - - - - - - - - - -    
   def set_speed_rough
   # - - - - - - - - - - - - - - - -     
     set_speed(@speed_normal)
   end # meth

   # - - - - - - - - - - - - - - - -    
   def set_speed_finish
   # - - - - - - - - - - - - - - - -     
     set_speed(@speed_finish)
   end # meth



   # - - - - - - - - - - - - - - - -
   def home
   # - - - - - - - - - - - - - - - - 
     if (@cx == @retract_depth) && (@cy == 0) && (@cz == 0)
       @no_move_count += 1
     else
       retract
       print "G00 X0 Y0 (HOME) \n"
       @cx = 0
       @cy = 0
     end #if
   end #meth



   # - - - - - - - - - - - - - - - -
   def min_y=(aObj)
   # - - - - - - - - - - - - - - - - 
     @min_y  = aObj
   end #meth
   # - - - - - - - - - - - - - - - -
   def min_y
   # - - - - - - - - - - - - - - - - 
     return @min_y
   end # meth
   # - - - - - - - - - - - - - - - -
   def max_y=(aObj)
   # - - - - - - - - - - - - - - - - 
     @max_y  = aObj
   end #meth
   # - - - - - - - - - - - - - - - -
   def max_y
   # - - - - - - - - - - - - - - - - 
     return @max_y
   end # meth

   # - - - - - - - - - - - - - - - -
   def max_x_move=(aObj)
   # - - - - - - - - - - - - - - - - 
     @max_x_move = aObj
     @max_x = @max_x_move + @min_x
   end #meth


   # - - - - - - - - - - - - - - - -
   def min_x=(aObj)
   # - - - - - - - - - - - - - - - - 
     @min_x  = aObj
     @max_x = @max_x_move + @min_x
   end #meth

   # - - - - - - - - - - - - - - - -
   def min_x
   # - - - - - - - - - - - - - - - - 
     return @min_x
   end # meth

   # maximum allowed movement range
   # on the x axis.  This is a read
   # only variable because it is 
   # automatically calcualted by
   # adding @max_x_move + @min_x.
   # - - - - - - - - - - - - - - - -
   def max_x
   # - - - - - - - - - - - - - - - - 
     return @max_x
   end # meth



   # - - - - - - - - - - - - - - - -
   def min_a=(aObj)
   # - - - - - - - - - - - - - - - - 
     @min_z  = aObj
   end #meth
   # - - - - - - - - - - - - - - - -
   def min_z
   # - - - - - - - - - - - - - - - - 
     return @min_z
   end # meth
   # - - - - - - - - - - - - - - - -
   def max_z=(aObj)
   # - - - - - - - - - - - - - - - - 
     @max_z  = aObj
   end #meth
   # - - - - - - - - - - - - - - - -
   def max_z
   # - - - - - - - - - - - - - - - - 
     return @max_z
   end # meth





   # - - - - - - - - - - - - - - - -     
   def move_fast(xo,yo=@cy,zo=@cz)
   # - - - - - - - - - - - - - - - - 
     if (xo == @cx) && yo = @cz  && (zo == @cz)
       @no_move_count += 1
     else
       if (xo > @max_x)
         xo = @max_x
       elsif (xo < @min_x)
         xo = @min_x
       end #if
       if (yo > @max_y)
         yo = @max_y
       elsif (yo < @min_y)
         yo = @min_y
       end #if
       if (zo > @max_z)
         zo = @max_z
       elsif (zo < @min_z)
         zo = @min_z
       end #if
       print "G00 X",sprintf("%8.3f", xo),  " Y",sprintf("%8.3f", yo), " Z",sprintf("%8.3f", zo)  , " (move fast)\n"
       @cx = xo
       @cy = yo
       @cz = zo 
     end # if
   end #meth



   # Mill a rectangle between the coordinates
   # specified.   The caller is responsible for either
   # pre-positioning the bit inside the rectangle
   # or retracting it prior to the call.   This method
   # does not supply any layer or flute length support
   # because it is normally called by higher level methods 
   # that do.    Used current speed which can be changed
   # by calling set_speed prior to calling this method
   # - - - - - - - - - - - - - - - -     
   def mill_rect(lx, ly, mx, my, depth, adjust_for_bit_radius=false)
   # - - - - - - - - - - - - - - - -
     # if needed swap lx and mx to ensure that
     # lx is the smaller number
      if (lx > mx)
        tt = lx
        llx = mx
        mx = tt
     end
      
     # if needed swap ly,my to make sure that
     # ly is the lower number.
     if (ly > my)
       tt = ly
       lly = my
       my = tt
     end


     if (adjust_for_bit_radius == true)
       lbr = bit_radius
       llx = lx + br
       lly = ly + br
       mx = mx - br
       my = my - br
     end

     # if our point is already inside the defined rectangle
     # then we assume it is safe to move without a retract
     if ((@cx < lx) || (@cx > mx) || (@cy < ly) || (@cy > my))
        rretract()
     end

     # walk around the perimiter of the sqare
     move(lx,ly)
     plung(depth)  # if already at correct depth will
                         # just ignore
     move(lx,ly)
     move(lx,my)
     move(mx,my)
     move(mx,ly)
     move(lx,ly)

   end #meth

   # Mill a simple rectangle the diameter of the
   # the milling bit that follows the coordinates
   # speicified.   The caller is responsible to 
   # either pre-position the bit or retract the 
   # bit prior to calling.   This method
   # does not supply any layer or flute length support
   # because it is normally called by higher level methods 
   # that do.
   # - - - - - - - - - - - - - - - -     
   def mill_rect_centered(scx,scy,width,length, depth)
   # - - - - - - - - - - - - - - - -i
     lx = to_f(scx) - (width  / 2)  
     ly = to_f(scy) - (length / 2)
     mx = to_f(scx) + (width  / 2)
     my = to_f(scy) + (length / 2)
     mill_rect(lx,ly,mx,my, depth)
   end #meth

   # uses simple  trig function to calculate
   # distance between two points
   # - - - - - - - - - - - - - - - - - -
   def calc_distance(x1,y1,x2=@cx,y2=@cy)
   # - - - - - - - - - - - - - - - - - -
      #print "(calc_distance x1=", x1, " y1=", y1, " x2=", x2, " y2=", y2, ")\n"
      # TODO: Figure out how to call the cncGeometry version
      #   even though we have a name conflict.
      dx = (to_f(x1) - x2).abs
      dy = (to_f(y1) - y2).abs
      tdist =  Math.sqrt((dx*dx) + (dy*dy))
      return tdist
   end #meth

   

   # returns a point adjusted to 
   # fit the current max extents 
   # active for the mill. 
   # - - - - - - - - - - - - - - - - - -
   def apply_limits(xo, yo, zo=nil)
   # - - - - - - - - - - - - - - - - - -
     if (yo > @max_y)
       yo = @max_y
     end #if
     if (yo < @min_y)
       yo = @min_y
     end #if


     if (xo > @max_x)
       xo = @max_x
     end #if
     if (xo < @min_x)
       xo = @min_x
     end #if

     if (zo != nil)
       if (zo > @max_z)
         zo = @max_z
       end #if
       if (zo < @min_z)
         zo = @min_z
       end #if
     end #if


     np = CNCPoint.new(xo,yo,zo)
     return np
   end #meth



end # class
