# steel-rotozip-0.112
# used to adjust the recomended defaults
# for the material currently active.
#

rpm_adjust = 2.0
diam  = 0.112
per_tooth_adjust = 1.0
cut_depth_adjust = 1.0
sfm_adjust       = 1.0

