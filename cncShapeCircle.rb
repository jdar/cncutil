# cncShapeCircle.rb
#  See (C) notice in http://cncutil.org/cnc/CNCUtil/license.txt for license and copyright.

require 'cncMill'
require 'cncBit'
require 'cncMaterial'
require 'cncGeometry'
require 'cncShapeBase'
require 'cncShapeArc'

 #  PRIMITIVE: Caller is responsible to move the bit to a point inside
 #  the circle and to plung the bit the required depth
 #  prior to calling this function.   
 #
 #  Does not handle multiple passes for depth.  If multiple passes may
 #  be needed then use the the CNCShapeCircle instead.
 # - - - - - - - - - - - - - - - - - -
 def mill_circle(mill, x,y, diam, depth=@cz, adjust_for_bit_radius=false)
 # - - - - - - - - - - - - - - - - - -
   #print "(mill circle x=", sprintf("%8.3f",x), " y=", sprintf("%8.3f",y), " diam=", sprintf("%8.3f",diam), " depth=", depth, ")\n"
   print sprintf("(mill circle x=%8.3f y=%8.3f diam=%8.3f  depth=%8.3f)\n", x,y,diam,depth)
   cradius = diam / 2.0

   # If we need to adjust for radius then we subtract our
   # bit radius from our circle diameter prior to milling the
   # arc segments.
   if (adjust_for_bit_radius == true)
     cradius -= mill.bit_radius
   end #if

   # calculate if the current cx,cy coordinates are outside
   # the circle and if so perform an automatic retract
   tRad = mill.calc_distance(mill.cx, mill.cy, x,y)
   if (tRad > (cradius + 0.001))  
     # add 0.001 to allow for rounding error
     print "(AUTO RETRACT in Mill Circle tRad=", tRad, "circle radius = ", cradius, ")\n"
     mill.retract()
   end #if


   # TODO:  It is supposed to be possible to mill the 
   #  complete circle with a single call but could not 
   #  make it work with EMC so needs further experiments
   # 
   # TODO: Double check this to for the mill climb direction
   # 

   # Mill the 4 arc segments. 
   mill.move(x,y-cradius)
   mill.plung(depth)
   mill_arc(mill, x + cradius, y, cradius, mill.speed, "G03")
   mill_arc(mill, x, y + cradius, cradius, mill.speed, "G03")
   mill_arc(mill, x - cradius, y, cradius, mill.speed, "G03")
   mill_arc(mill, x, y - cradius, cradius, mill.speed, "G03")
 end #meth



# *****************************************
class CNCShapeCircle 
  # *****************************************
  include  CNCShapeBase
  extend  CNCShapeBase

  # - - - - - - - - - - - - - - - - - - 
  def initialize(aMill,x=0.0,y=0.0,depth=nil,island_diam=0.0)
    # - - - - - - - - - - - - - - - - - -
    base_init(aMill, x,y,depth)
    @island_diam = island_diam
    return self
  end #meth


  # basically calls the mill operation for
  # using the interal values for this circle
  # object.
  # - - - - - - - - - - - - - - - -
  def do_mill
    mill_pocket(@x, @y, @diam, @depth, @island_diam)
  end


  # Performs a basic millng operation
  # according to the specifications supplied.
  # This does not change the internal circle
  # object specifications.   This method will 
  # properly handle multiple passes required
  # to reach the requested depth.
  # - - - - - - - - - - - - - - - -     
  def mill_pocket(x,y,diam, depth=@mill.mill_depth, island_diam=0)
    # - - - - - - - - - - - - - - - -     
    if depth.abs <= cut_depth_rough.abs
      #print "(simple mill square depth", depth, ")", "\n"
      print "(mill circle as simple)\n"
      mill_pocket_s(x,y,diam, depth, island_diam)
      return
    else
      dd = 0
      dd = 0 - cut_depth_rough.abs      
      while true
        mill_pocket_s(x,y,diam, dd, island_diam)
        if (dd <= depth)
          break
        end #if
        dd -= cut_depth_rough.abs
        if (dd < depth)
          dd = depth
        end #if
      end #while
    end #else
    self
  end #meth


  #  mill a circle of a given diameter at 
  #  the specified x,y location to a specified depth
  #  leaving an island of the specified height in the
  #  center.    
  # - - - - - - - - - - - - - - - - - -
  def mill_pocket_s(x,y,diam,depth=@mill.mill_depth,island_diam=0)
    # - - - - - - - - - - - - - - - - - -
    print "(L445: Circle.mill_pocket x=",x, " y=",y, " diam=", diam, " depth=", depth, " island_diam=", island_diam,")\n"
    cradius = (diam / 2) - bit_radius
    island_radius= island_diam / 2

    # If my bit is outside the pocket or if the 
    # bit is inside my island then I need to retract
    # the bit prior to moving over to start the milling
    cdist = @mill.calc_distance(x,y)
    if cdist > (cradius + 0.001)  || cdist < (island_radius + bit_radius)
      print "(AUTO retract in Mill circle pocket)\n"
      @mill.retract()
    end #if


    # Start a little bit away from the 
    # island trace it using a slow speed
    # and small finish cut increments
    trad = island_radius + bit_radius 
    #print "(L454: trad=", trad
    if (trad > bit_diam)
      xrad = island_radius  + cut_increment_finish * 3
      if xrad >= cradius
        xrad = cradius
      end #if
      
      txrad = xrad
      @mill.set_speed_finish()
      @mill.move(x,y-xrad)
      @mill.plung(depth)
      while (xrad >= trad)
        #print "(L464: xrad=", xrad, " trad=", trad, " cut_increment_finish=", cut_increment_finish, ")\n"
        @mill.move(x,y-xrad) # prevent auto retract
        mill_circle(mill, x,y,xrad*2,0, depth)  
        xrad -= cut_increment_finish
      end #while
      # work from the island out towards the perimiter
      # of the circle
      trad = txrad
    end #if     
    @mill.move(x,y-trad) 
    @mill.plung(depth)
    rad_inc = cut_increment_rough
    @mill.set_speed_rough() 
    small_set = false
    
    
    # for small holes we have to
    # override the defaults or the code
    # will cause a simple plung and retract
    if ((trad >= cradius) and (diam > bit_diam))
      trad = cradius / 3
      small_set = true
      rad_inc = cradius / 3
    end #if  
    
    #print "(L489: cradius=", cradius,  " trad =", trad, ")\n"
    # Mill out the main circular pocket
    while(trad <= cradius)
      tdiam = trad * 2
      #print "(L498: trad=", trad, " cradius=", cradius," rad_inc=", rad_inc,")\n"
      @mill.move(x, y - trad) # prevent auto retract
      mill_circle(mill,x,y,tdiam, depth)
      
      if (tdiam == cradius)
        break # have finished this process
      end #if

      if (small_set == false) and ((trad + rad_inc) >= cradius)
        rad_inc = cut_increment_finish
        small_set = 1
        @mill.set_speed_finish()
       end #if
       trad += rad_inc

       if trad > cradius
          #print "(L504: make finish cut )\n"
          tdiam = cradius * 2
          @mill.set_speed_finish()
          @mill.move(x,y-cradius) # prevent auto retract
          mill_circle(mill,x,y,tdiam,depth)
          break
       end #if
    end #while
    self
  end #meth
end #class


