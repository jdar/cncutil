# cncMaterial.rb
#  See (C) notice in http://cncutil.org/cnc/CNCUtil/license.txt for license and copyright.
require 'cncBit'
module CNC

  # *****************************************
  class Material
  # *****************************************
     # - - - - - - - - - - - - - - - -
     def initialize
     # - - - - - - - - - - - - - - - -   
       @width = 4
       @length= 4
       @height= 4
       @max_speed = 15
     end  #init
  end #class
end #module